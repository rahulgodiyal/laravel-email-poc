<?php

use App\Mail\TestMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/email', function () {
    try {
        Mail::to(config('mail.to.address'), config('mail.to.name'))->send(new TestMail());
        return "Mail Sent Successfully";
    } catch (Exception $e) {
        return $e->getMessage();
    } 
});